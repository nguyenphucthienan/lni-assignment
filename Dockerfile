FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/*.jar
ENV AWS_ACCESS_KEY_ID = AWS_ACCESS_KEY_ID
ENV AWS_SECRET_ACCESS_KEY = AWS_SECRET_ACCESS_KEY

COPY ${JAR_FILE} app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]
