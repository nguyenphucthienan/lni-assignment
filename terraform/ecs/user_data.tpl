#!/bin/bash

# Update all packages
sudo apt-get update

# Adding cluster name in ecs config
sudo echo "ECS_CLUSTER=${ecs_cluster_name}" >> /etc/ecs/ecs.config
