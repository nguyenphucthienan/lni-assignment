resource "aws_launch_configuration" "launch_configuration" {
  name                        = "lni-assignment-terraform-lc"
  image_id                    = "ami-039ec8fc674496137"
  instance_type               = "t2.micro"
  iam_instance_profile        = aws_iam_instance_profile.ecs_instance_profile.name
  key_name                    = var.key_name
  security_groups             = [aws_security_group.ec2_sg.id]
  associate_public_ip_address = true
  user_data                   = data.template_file.user_data.rendered
  lifecycle {
    create_before_destroy = true
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    ecs_cluster_name = var.ecs_cluster_name
  }
}

resource "aws_autoscaling_group" "asg" {
  name                      = "lni-assignment-terraform-asg"
  launch_configuration      = aws_launch_configuration.launch_configuration.name
  min_size                  = 1
  max_size                  = 1
  desired_capacity          = 1
  health_check_type         = "ELB"
  health_check_grace_period = 300
  vpc_zone_identifier       = var.subnet_ids
  target_group_arns         = [aws_lb_target_group.lb_target_group.arn]
  protect_from_scale_in     = true
  lifecycle {
    create_before_destroy = true
  }
}
