resource "aws_ecs_cluster" "cluster" {
  name               = var.ecs_cluster_name
  capacity_providers = [aws_ecs_capacity_provider.capacity_provider.name]
}

resource "aws_ecs_capacity_provider" "capacity_provider" {
  name = "lni-assignment-terraform-cp"
  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.asg.arn
    managed_termination_protection = "ENABLED"
    managed_scaling {
      status          = "ENABLED"
      target_capacity = 85
    }
  }
}

resource "aws_ecs_task_definition" "task_definition" {
  family                = var.container_name
  container_definitions = data.template_file.task_definition.rendered
  network_mode          = "bridge"
}

data "template_file" "task_definition" {
  template = file("${path.module}/container-definition.json")
  vars = {
    repository_url = var.repository_url
    container_name = var.container_name
    awslogs_region = var.region
  }
}

resource "aws_ecs_service" "service" {
  name            = "lni-assignment-terraform-service"
  launch_type     = "EC2"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  desired_count   = 1
  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.lb_target_group.arn
    container_name   = var.container_name
    container_port   = 8080
  }
  lifecycle {
    ignore_changes = [desired_count]
  }
  depends_on = [aws_lb_listener.lb_listener]
}
