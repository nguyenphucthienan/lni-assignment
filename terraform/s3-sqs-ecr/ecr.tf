resource "aws_ecr_repository" "repository" {
  name                 = "lni-assignment-terraform-repository"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository_policy" "ecr-policy" {
  repository = aws_ecr_repository.repository.name
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "Add ECR access to repository",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
        ]
      }
    ]
  }
  EOF
}

output "repo-url" {
  value = aws_ecr_repository.repository.repository_url
}
