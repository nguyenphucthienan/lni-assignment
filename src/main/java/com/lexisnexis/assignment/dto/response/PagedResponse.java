package com.lexisnexis.assignment.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PagedResponse<T> {
    private List<T> data;

    private Pagination pagination;

    public PagedResponse(
            List<T> data,
            int page,
            int size,
            long totalElements,
            int totalPages,
            boolean first,
            boolean last
    ) {
        this.data = data;
        this.pagination = new Pagination(page, size, totalElements, totalPages, first, last);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Pagination {
        private int page;

        private int size;

        private long totalElements;

        private int totalPages;

        private boolean first;

        private boolean last;
    }
}
