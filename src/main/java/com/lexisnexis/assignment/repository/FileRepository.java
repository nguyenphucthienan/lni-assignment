package com.lexisnexis.assignment.repository;

import com.lexisnexis.assignment.entity.FileEntity;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FileRepository extends PagingAndSortingRepository<FileEntity, String> {
    @EnableScan
    @EnableScanCount
    Page<FileEntity> findAll(Pageable pageable);
}
