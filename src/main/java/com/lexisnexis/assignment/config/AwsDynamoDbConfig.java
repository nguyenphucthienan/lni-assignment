package com.lexisnexis.assignment.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@EnableDynamoDBRepositories(basePackages = "com.lexisnexis.assignment.repository")
public class AwsDynamoDbConfig {
    private final AWSCredentialsProvider awsCredentials;

    @Value("${cloud.aws.region.static}")
    private String region;

    @Autowired
    public AwsDynamoDbConfig(AWSCredentialsProvider awsCredentials) {
        this.awsCredentials = awsCredentials;
    }

    @Primary
    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        return AmazonDynamoDBClientBuilder
                .standard()
                .withCredentials(awsCredentials)
                .withRegion(region)
                .build();
    }
}
