package com.lexisnexis.assignment.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class AwsS3Config {
    private final AWSCredentialsProvider awsCredentials;

    @Value("${cloud.aws.region.static}")
    private String region;

    @Autowired
    public AwsS3Config(AWSCredentialsProvider awsCredentials) {
        this.awsCredentials = awsCredentials;
    }

    @Primary
    @Bean
    public AmazonS3 amazonS3Client() {
        return AmazonS3ClientBuilder
                .standard()
                .withRegion(region)
                .withCredentials(awsCredentials)
                .build();
    }
}
