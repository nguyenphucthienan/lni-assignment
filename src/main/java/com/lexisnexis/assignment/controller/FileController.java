package com.lexisnexis.assignment.controller;

import com.lexisnexis.assignment.dto.FileDto;
import com.lexisnexis.assignment.dto.response.FileResponse;
import com.lexisnexis.assignment.dto.response.PagedResponse;
import com.lexisnexis.assignment.service.FileService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/files")
public class FileController {
    private final FileService fileService;

    private final ModelMapper modelMapper;

    @Autowired
    public FileController(FileService fileService, ModelMapper modelMapper) {
        this.fileService = fileService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public PagedResponse<FileResponse> getFiles(Pageable pageable) {
        final PagedResponse<FileDto> fileDtoPagedResponse = fileService.getFiles(pageable);
        final List<FileResponse> fileResponses = modelMapper.map(
                fileDtoPagedResponse.getData(), new TypeToken<List<FileResponse>>() {
                }.getType());
        return new PagedResponse<>(fileResponses, fileDtoPagedResponse.getPagination());
    }

    @GetMapping("/{id}")
    public FileResponse getFile(@PathVariable String id) {
        final FileDto fileDto = fileService.getFile(id);
        return modelMapper.map(fileDto, FileResponse.class);
    }
}
