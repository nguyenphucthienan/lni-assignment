package com.lexisnexis.assignment.controller;

import com.lexisnexis.assignment.dto.UploadFileDto;
import com.lexisnexis.assignment.dto.response.UploadFileResponse;
import com.lexisnexis.assignment.service.AwsS3Service;
import com.lexisnexis.assignment.util.FileUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/s3")
public class AwsS3Controller {
    private final AwsS3Service awsS3Service;

    private final ModelMapper modelMapper;

    @Autowired
    public AwsS3Controller(AwsS3Service awsS3Service, ModelMapper modelMapper) {
        this.awsS3Service = awsS3Service;
        this.modelMapper = modelMapper;
    }

    @PostMapping(value = "/upload")
    public UploadFileResponse uploadFile(@RequestPart(value = "file") final MultipartFile multipartFile) {
        final UploadFileDto uploadFileDto = awsS3Service.uploadFile(multipartFile);
        return modelMapper.map(uploadFileDto, UploadFileResponse.class);
    }

    @GetMapping(value = "/download")
    public ResponseEntity<byte[]> downloadFile(@RequestParam String fileName) {
        final byte[] fileContent = awsS3Service.downloadFile(fileName);
        return ResponseEntity
                .ok()
                .contentType(FileUtils.getContentTypeFromFileName(fileName))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(fileContent);
    }
}
