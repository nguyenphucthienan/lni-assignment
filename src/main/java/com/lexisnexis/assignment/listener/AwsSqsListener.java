package com.lexisnexis.assignment.listener;

import com.amazonaws.services.s3.event.S3EventNotification;
import com.lexisnexis.assignment.dto.FileDto;
import com.lexisnexis.assignment.service.AwsS3Service;
import com.lexisnexis.assignment.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Component
public class AwsSqsListener {
    private final AwsS3Service awsS3Service;

    private final FileService fileService;

    @Autowired
    public AwsSqsListener(AwsS3Service awsS3Service, FileService fileService) {
        this.awsS3Service = awsS3Service;
        this.fileService = fileService;
    }

    @SqsListener(value = "${application.queue.name}", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void onS3UploadEvent(final S3EventNotification eventNotification) {
        final String fileName = eventNotification.getRecords().get(0).getS3().getObject().getKey();
        final String fileContent = new String(awsS3Service.downloadFile(fileName));
        final FileDto fileDto = new FileDto();
        fileDto.setName(fileName);
        fileDto.setContent(fileContent);
        fileService.saveFile(fileDto);
    }
}
