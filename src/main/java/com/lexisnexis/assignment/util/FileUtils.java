package com.lexisnexis.assignment.util;

import org.springframework.http.MediaType;

public class FileUtils {
    public static MediaType getContentTypeFromFileName(String fileName) {
        String[] fileSplit = fileName.split("\\.");
        String fileExtension = fileSplit[fileSplit.length - 1];
        switch (fileExtension) {
            case "txt":
                return MediaType.TEXT_PLAIN;
            case "png":
                return MediaType.IMAGE_PNG;
            case "jpg":
                return MediaType.IMAGE_JPEG;
            default:
                return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
}
