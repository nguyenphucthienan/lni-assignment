package com.lexisnexis.assignment.service;

import com.lexisnexis.assignment.dto.UploadFileDto;
import org.springframework.web.multipart.MultipartFile;

public interface AwsS3Service {
    UploadFileDto uploadFile(MultipartFile file);

    byte[] downloadFile(String fileName);
}
