package com.lexisnexis.assignment.service;

import com.lexisnexis.assignment.dto.FileDto;
import com.lexisnexis.assignment.entity.FileEntity;
import com.lexisnexis.assignment.repository.FileRepository;
import com.lexisnexis.assignment.dto.response.PagedResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileServiceImpl implements FileService {
    private final FileRepository fileRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository, ModelMapper modelMapper) {
        this.fileRepository = fileRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public PagedResponse<FileDto> getFiles(final Pageable pageable) {
        final Page<FileEntity> fileEntityPage = fileRepository.findAll(pageable);
        final List<FileDto> fileDtos = modelMapper.map(fileEntityPage.getContent(), new TypeToken<List<FileDto>>() {
        }.getType());
        return new PagedResponse<>(
                fileDtos,
                fileEntityPage.getNumber(),
                fileEntityPage.getSize(),
                fileEntityPage.getTotalElements(),
                fileEntityPage.getTotalPages(),
                fileEntityPage.isFirst(),
                fileEntityPage.isLast()
        );
    }

    @Override
    public FileDto getFile(final String id) {
        final FileEntity fileEntity = fileRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("File with ID " + id + " not found"));
        return modelMapper.map(fileEntity, FileDto.class);
    }

    @Override
    public FileDto saveFile(final FileDto fileDto) {
        final FileEntity fileEntity = modelMapper.map(fileDto, FileEntity.class);
        final FileEntity savedFileEntity = fileRepository.save(fileEntity);
        return modelMapper.map(savedFileEntity, FileDto.class);
    }
}
