package com.lexisnexis.assignment.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import com.lexisnexis.assignment.dto.UploadFileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class AwsS3ServiceImpl implements AwsS3Service {
    private final Logger logger = LoggerFactory.getLogger(AwsS3ServiceImpl.class);

    private final AmazonS3 amazonS3Client;

    @Autowired
    public AwsS3ServiceImpl(AmazonS3 amazonS3Client) {
        this.amazonS3Client = amazonS3Client;
    }

    @Value("${application.bucket.name}")
    private String bucketName;

    @Override
    public UploadFileDto uploadFile(MultipartFile multipartFile) {
        try {
            final String fileName = UUID.randomUUID() + "-" + multipartFile.getOriginalFilename();
            final ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(multipartFile.getSize());
            final PutObjectRequest request = new PutObjectRequest(bucketName, fileName, multipartFile.getInputStream(), metadata);
            amazonS3Client.putObject(request);
            return UploadFileDto.builder().name(fileName).build();
        } catch (IOException exception) {
            logger.error("IOException: {}", exception.getMessage());
        }
        return null;
    }

    @Override
    public byte[] downloadFile(String fileName) {
        try {
            final S3Object s3object = amazonS3Client.getObject(new GetObjectRequest(bucketName, fileName));
            final InputStream inputStream = s3object.getObjectContent();
            final byte[] objectContent = IOUtils.toByteArray(inputStream);
            s3object.close();
            return objectContent;
        } catch (IOException exception) {
            logger.error("IOException: {}", exception.getMessage());
        }
        return null;
    }
}
