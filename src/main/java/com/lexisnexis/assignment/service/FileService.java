package com.lexisnexis.assignment.service;

import com.lexisnexis.assignment.dto.FileDto;
import com.lexisnexis.assignment.dto.response.PagedResponse;
import org.springframework.data.domain.Pageable;

public interface FileService {
    PagedResponse<FileDto> getFiles(final Pageable pageable);

    FileDto getFile(final String id);

    FileDto saveFile(final FileDto fileDto);
}
